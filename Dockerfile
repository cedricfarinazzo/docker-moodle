FROM debian

# Update database
RUN apt-get update -yy

# Installe dependencies
RUN apt-get install -yy wget curl git zip
RUN apt-get install -yy apache2 w3m
RUN apt-get install -yy php libapache2-mod-php
RUN apt-get install -yy php-gd php-mysql php-bz2 php-json php-curl php-zip php-xml
RUN apt-get install -yy php-intl php-mbstring php-xmlrpc php-soap

# Setup apache2
WORKDIR /etc/apache2/sites-available
ADD 001-moodle.conf .
RUN a2dissite 000-default
RUN a2ensite 001-moodle

ADD start.sh /etc/apache2/

EXPOSE 80
EXPOSE 443

# Add php.ini
ADD php.ini /etc/php/apache2/php.ini

# Install moodle from github
ARG MOODLE_VERSION=3.8.1
WORKDIR /var/www
RUN mkdir moodle
WORKDIR /var/www/moodle

RUN wget https://github.com/moodle/moodle/archive/v$MOODLE_VERSION.zip
RUN unzip -o v$MOODLE_VERSION.zip
RUN rm -rf v$MOODLE_VERSION.zip
RUN ln -s moodle-$MOODLE_VERSION moodle
RUN chown -R www-data:www-data /var/www/moodle

VOLUME /var/www/moodle

CMD /etc/apache2/start.sh
